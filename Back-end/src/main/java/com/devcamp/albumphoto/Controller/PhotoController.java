package com.devcamp.albumphoto.Controller;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.albumphoto.Model.Album;
import com.devcamp.albumphoto.Model.Photo;
import com.devcamp.albumphoto.Repository.AlbumRepository;
import com.devcamp.albumphoto.Repository.PhotoRepository;

@RestController
@CrossOrigin
public class PhotoController {
    @Autowired
    PhotoRepository photoRepository;

    @Autowired
    AlbumRepository albumRepository;

    // API để lấy toàn bộ photo
    @GetMapping("/photos")
    public ResponseEntity<List<Photo>> getAllPhoto() {
        try {
            return new ResponseEntity<>(photoRepository.findAll(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API lấy thông tin photo thông qua id
    @GetMapping("/photo/{id}")
    public ResponseEntity<Photo> getPhotoById(@PathVariable("id") long id) {
        try {
            return new ResponseEntity<>(photoRepository.findById(id).get(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API lấy ra toàn bộ photo của 1 album thông qua Id
    @GetMapping("/album/{albumId}/photos")
    public ResponseEntity<Set<Photo>> getAllPhotoOfAlbum(@PathVariable("albumId") long albumId) {
        try {
            Album album = albumRepository.findById(albumId).get();
            return new ResponseEntity<>(album.getPhotos(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Hàm tạo mới 1 photo
    @PostMapping("/album/{albumId}/photos")
    public ResponseEntity<Photo> createPhoto(@PathVariable("albumId") long albumId, @RequestBody Photo pPhoto) {
        try {
            Album album = albumRepository.findById(albumId).get();
            pPhoto.setAlbum(album);
            pPhoto.setNgayTao(new Date());
            return new ResponseEntity<>(photoRepository.save(pPhoto), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Hàm sửa thông tin 1 photo
    @PutMapping("/album/{albumId}/photo/{photoId}")
    public ResponseEntity<Photo> updatePhoto(@PathVariable("photoId") long photoId, @PathVariable("albumId") long albumId, @RequestBody Photo pPhoto) {
        try {
            Photo photo = photoRepository.findById(photoId).get();
            Album album = albumRepository.findById(albumId).get();
            photo.setAlbum(album);
            photo.setLinkPhoto(pPhoto.getLinkPhoto());
            photo.setPhotoCode(pPhoto.getPhotoCode());
            photo.setPhotoName(pPhoto.getPhotoName());  
            photo.setDescription(pPhoto.getDescription());
            return new ResponseEntity<>(photoRepository.save(photo), HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Hàm xóa 1 photo
    @DeleteMapping("/photo/{id}")
    public ResponseEntity<Album> deletePhoto(@PathVariable("id") long id) {
        try {
            photoRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Hàm xóa tất cả photo
    @DeleteMapping("/photos")
    public ResponseEntity<Album> deleteAllPhotos() {
        try {
            photoRepository.deleteAll();;
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
