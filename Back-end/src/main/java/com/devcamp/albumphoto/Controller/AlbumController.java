package com.devcamp.albumphoto.Controller;

import java.util.Date;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.albumphoto.Model.Album;
import com.devcamp.albumphoto.Repository.AlbumRepository;

@RestController
@CrossOrigin
public class AlbumController {
    @Autowired
    AlbumRepository albumRepository;

    // API để lấy toàn bộ album
    @GetMapping("/albums")
    public ResponseEntity<List<Album>> getAllAlbum() {
        try {
            return new ResponseEntity<>(albumRepository.findAll(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API lấy thông tin album thông qua id
    @GetMapping("/album/{id}")
    public ResponseEntity<Album> getAlbumById(@PathVariable("id") long id) {
        try {
            return new ResponseEntity<>(albumRepository.findById(id).get(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // API tạo mới 1 album
    @PostMapping("/albums")
    public ResponseEntity<Album> createAlbum(@RequestBody Album pAlbum) {
        try {
            pAlbum.setNgayTao(new Date());
            return new ResponseEntity<>(albumRepository.save(pAlbum), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Hàm cập nhật 1 album
    @PutMapping("/album/{id}")
    public ResponseEntity<Album> updateAlbum(@RequestBody Album pAlbum, @PathVariable("id") long id) {
        try {
            Album album = albumRepository.findById(id).get();
            album.setAlbumCode(pAlbum.getAlbumCode());
            album.setAlbumName(pAlbum.getAlbumName());
            album.setDescription(pAlbum.getDescription());
            return new ResponseEntity<>(albumRepository.save(album), HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Hàm xóa 1 album
    @DeleteMapping("/album/{id}")
    public ResponseEntity<Album> deleteAlbum(@PathVariable("id") long id) {
        try {
            albumRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Hàm xóa tất cả album
    @DeleteMapping("/albums")
    public ResponseEntity<Album> deleteAllAlbums() {
        try {
            albumRepository.deleteAll();;
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
