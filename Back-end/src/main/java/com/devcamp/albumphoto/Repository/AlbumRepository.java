package com.devcamp.albumphoto.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.albumphoto.Model.Album;

public interface AlbumRepository extends JpaRepository<Album, Long>{
    
}
