package com.devcamp.albumphoto.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.albumphoto.Model.Photo;

public interface PhotoRepository extends JpaRepository<Photo, Long> {
    
}
