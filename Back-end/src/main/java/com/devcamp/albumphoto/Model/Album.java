package com.devcamp.albumphoto.Model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "albums")
public class Album {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotEmpty(message = "album code không được bỏ trống!")
    @JoinColumn(name = "album_code", unique = true)
    private String albumCode;

    @NotEmpty(message = "Tên album không được bỏ trống!")
    @JoinColumn(name = "album_name")
    private String albumName;

    @NotEmpty(message = "Mô tả không được bỏ trống!")
    private String description;

    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "dd-MM-yyyy")
    @JoinColumn(name = "ngay_tao")
    private Date ngayTao;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "album")
    private Set<Photo> photos;

    public Album() {
    }

    public Album(@NotEmpty(message = "album code không được bỏ trống!") String albumCode,
            @NotEmpty(message = "Tên album không được bỏ trống!") String albumName,
            @NotEmpty(message = "Mô tả không được bỏ trống!") String description, Date ngayTao, Set<Photo> photos) {
        this.albumCode = albumCode;
        this.albumName = albumName;
        this.description = description;
        this.ngayTao = ngayTao;
        this.photos = photos;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAlbumCode() {
        return albumCode;
    }

    public void setAlbumCode(String albumCode) {
        this.albumCode = albumCode;
    }

    public String getAlbumName() {
        return albumName;
    }

    public void setAlbumName(String albumName) {
        this.albumName = albumName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(Date ngayTao) {
        this.ngayTao = ngayTao;
    }

    public Set<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(Set<Photo> photos) {
        this.photos = photos;
    }

    
}
